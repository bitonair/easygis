#ifndef MAPAREA_H
#define MAPAREA_H


#include <QAbstractGraphicsShapeItem>
#include <QPolygonF>
#include <QPen>
namespace EasyGIS {
class MapLayer;
/**
 * @brief 绘制区域
 */
class Maparea : public QAbstractGraphicsShapeItem
{
public:
    explicit Maparea(const MapLayer *layer, QGraphicsItem *parent = nullptr)
        : mLayer(layer), QAbstractGraphicsShapeItem(parent) {
        QPen pen(Qt::red);
        pen.setWidth(2);
        this->setPen(pen);
        QColor color(Qt::red);
        color.setAlpha(50);
        this->setBrush(color);
    }
    ~Maparea() override = default;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
    QRectF boundingRect() const override;
    void append(QPointF mkt_point);
    QPointF& lastMktpoint();
private:
    const MapLayer *mLayer;
    QPolygonF mkt_points;

};
}
#endif // MAPAREA_H
