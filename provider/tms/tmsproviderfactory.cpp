#include <provider/tms/tmsproviderfactory.h>
#include <provider/tms/ostnormalprovider.h>
#include <provider/tms/gaodenormalprovider.h>

namespace EasyGIS {

QHash<TmsProviders, LayerProvider *> TmsProviderFactory::mProviders{};

LayerProvider *
TmsProviderFactory::create(EasyGIS::TmsProviders provider) {
  LayerProvider *result = nullptr;
  switch (provider) {
    case OSTNormalMap:
      if (mProviders.contains(OSTNormalMap)) {
        result = mProviders.value(OSTNormalMap);
      } else {
        result = new OSTNormalProvider();
        mProviders.insert(OSTNormalMap, result);
      }
      break;
    case GaodeNormapMap:
      if (mProviders.contains(GaodeNormapMap)) {
        result = mProviders.value(GaodeNormapMap);
      } else {
        result = new GaodeNormalProvider();
        mProviders.insert(GaodeNormapMap, result);
      }
      break;
    default:break;
  }

  return result;
}

}

