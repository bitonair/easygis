#pragma once

#include <QString>
#include <QRectF>

#include <maplayer.h>
#include <mapcanvas.h>
#include <crs/webmercator.h>
#include <provider/tms/tmsproviderfactory.h>

namespace EasyGIS
{
  /**
   * tms服务所用图层
   */
  class TmsLayer : public MapLayer
  {
  public:
    TmsLayer(TmsProviders provider,
             const QString& id,
             MapCanvas* mapCanvas,
             CRS* crs = new WebMercator{});

  public:
    /**
     * 获取图层在当前zoom值的分辨率
     * @return 图层分辨率
     */
    double resolution() const override;

    /**
     * 获取图层的整个显示边界，一般情况下等同于坐标系的边界
     * @return 图层的显示区域
     */
    QRectF extent() const override;

    /**
     * 设置图层的zoom值
     * @param zoom 图层zoom值
     */
    void setZoomValue(int zoom) override;

    /**
     * 由rect和zoom值解析出该区域内的所有瓦片url路径，
     * 并将结果存储在tiles中
     * @param rect 区域（经纬度坐标）
     * @param zoom zoom值
     * @param tiles 结果
     * @param size 下载的瓦片大小
     * @return 是否解析成功
     */
    bool parseTiles(const QRectF& rect, int zoom, QHash<QPoint, QString>& tiles, QSize& size) const;

    /**
     * 图层所能支持的最大zoom值
     * @return 最大zoom值
     */
    virtual int maxZoom() const { return kMaxZoom; }

    /**
     * 图层所能支持的最小zoom值
     * @return 最小zoom值
     */
    virtual int minZoom() const { return kMinZoom; }

  private:
    static const int kMinZoom{4};
    static const int kMaxZoom{18};
  };
}
