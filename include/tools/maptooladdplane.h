#ifndef MAPTOOLADDPLANE_H
#define MAPTOOLADDPLANE_H
#include "maptool.h"
#include <QList>
namespace  EasyGIS{
class MapAutoplane;
class MapToolAddplane:public MapTool
{
public:
    explicit MapToolAddplane(MapCanvas *mapCanvas):MapTool(mapCanvas){}
    ~MapToolAddplane() override = default;
    void execute(QMouseEvent *event) override;
    void setup() override;
    void deSetup() override;
    QString id() override;
    inline QList<MapAutoplane*> getPlanes(){return planes;};
private:
    void addPlane(MapAutoplane*plane);
    void delePlane(MapAutoplane* plane);
private:
    QList<MapAutoplane*> planes;

};
}
#endif // MAPTOOLADDPLANE_H
