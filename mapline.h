#ifndef MAPLINE_H
#define MAPLINE_H

#include <QAbstractGraphicsShapeItem>
#include <QPolygonF>
#include <QPen>
namespace EasyGIS {
class MapLayer;
class Mapline:public QAbstractGraphicsShapeItem
{
public:
    explicit Mapline(const MapLayer *layer, QGraphicsItem *parent = nullptr)
        : mLayer(layer), QAbstractGraphicsShapeItem(parent) {
        QPen pen(Qt::red);
        pen.setWidth(2);
        this->setPen(pen);
        this->setBrush(QBrush(Qt::black));
    }
    ~Mapline() override = default;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
    QRectF boundingRect() const override;
    void append(QPointF mkt_point);
    QPointF& lastMktpoint();
private:
    const MapLayer *mLayer;
    QPolygonF mkt_points;
};
}

#endif // MAPLINE_H
