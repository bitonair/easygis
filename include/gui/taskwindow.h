#pragma once

#include "ui_taskwindow.h"
#include <QDialog>
#include <QWidget>
#include <QString>
#include <QRunnable>

#include <gui/mainwindow.h>
#include <layerprovider.h>
#include <pointxy.h>

namespace Ui{
class TaskWindow;
}

namespace EasyGIS{
struct TaskInfo{
  QString taskName;
  QString taskPath;
  QRectF extent;
  int zoom;
  const MapLayer *layer;
  QTableWidget *display;
};

class DownloadTask : public QObject,  public QRunnable {
 Q_OBJECT

 signals:
  void progressChanged(int row, int column, QString text);

 public:
  explicit DownloadTask(TaskInfo task, QObject *parent = nullptr)
      : mTaskInfo(std::move(task)), mRowId(), QObject(parent), QRunnable() {}
  ~DownloadTask() override = default;

 public:
  void run() override;

 protected:
  void generateCommonRow();
  void generateErrorRow();
  void generateSuccessRow();
  QString getExtentStr();

 protected:
  TaskInfo mTaskInfo;
  int mRowId;
};

class TaskWindow: public QDialog{
  Q_OBJECT
 public:
  explicit TaskWindow(QWidget * parent= nullptr);
    ~TaskWindow() override;

 protected:
  void setupWindow();
  void setupAction();
  void createTask();
  void setupSaveDir();
  void setupTaskName(const QString &text);
  bool taskInfoCheck();
  void setupZoomValue(int index);

 private:
  Ui::TaskWindow *mUi;
  MainWindow *mParent;

  QString mSavePath;
  QString mTaskName;
  int mZoomValue;
};

}