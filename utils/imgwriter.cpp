#include <QDebug>
#include <QPainter>
#include <QSize>

#include <utils/imgwriter.h>

namespace EasyGIS
{
  ImgWriter::ImgWriter(QString file, const QSize& size)
    : mFile(std::move(file))
  {
    QImage img{size, QImage::Format_RGB32};
    if (img.isNull()) {
      qCritical() << "存储图像创建失败";
      return;
    }

    Q_UNUSED(img.save(mFile, "TIFF"));
  }

  bool
  ImgWriter::write(const QPoint& pos, const QByteArray& data) const
  {
    QImage img{mFile};
    QPainter painter{&img};
    auto tile = QImage::fromData(data).convertToFormat(QImage::Format_RGB32);

    painter.drawImage(pos, tile);

    return img.save(mFile, "TIFF");
  }
}
