#include <QtCore/QString>

#include <tools/maptoolzoomout.h>
#include <include/tools/maptoolzoomout.h>

namespace EasyGIS
{

QString
MapToolZoomOut::id() {
  return QString{"zoomout_tool"};
}

void
MapToolZoomOut::setup() {
  /// do nothing
}

void
MapToolZoomOut::execute(QMouseEvent *event) {
  if(!(event->button() & Qt::LeftButton) || event->type() != QEvent::MouseButtonPress){
    return;
  }

  int zoom = mMapCanvas->zoomValue();
  --zoom;
  mMapCanvas->setZoomValue(zoom);
}

void
MapToolZoomOut::deSetup() {
  /// do nothing
}

}

