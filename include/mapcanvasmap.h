#pragma once

#include <QGraphicsItem>

namespace EasyGIS {

class MapLayer;

/**
 * 地图容器中的地图
 */
class MapCanvasMap : public QGraphicsItem {
 public:
  explicit MapCanvasMap(MapLayer *layer, QGraphicsItem *parent = nullptr)
      : mLayer(layer), QGraphicsItem(parent) {}
  ~MapCanvasMap() override = default;

 public:
  void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

  QRectF boundingRect() const override;

  /**
   * 设置图层显示区域
   * @param rect
   */
  void setViewExtent(const QRectF &rect);

 protected:
  /**
   * 所属图层
   */
  const MapLayer *mLayer;

  /**
   * 图层的显示区域
   */
  QRectF mViewExtent;
};

}
