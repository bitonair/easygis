#include <QDebug>
#include <proj_api.h>

#include <crs.h>

namespace EasyGIS {

PointXY
CRS::forward(const PointXY &point) const {
  auto pjCtx = pj_ctx_alloc();
  auto pjLonlat = pj_init_plus_ctx(pjCtx, "+proj=longlat +datum=WGS84 +no_defs");
  if (!pjLonlat) {
    qWarning() << "初始化wgs84坐标系失败=>" << pj_strerrno(pj_ctx_get_errno(pjCtx));
    pj_ctx_free(pjCtx);
    return PointXY{};
  }
  auto pjDest = pj_init_plus_ctx(pjCtx, proj4Def().toStdString().c_str());
  if (!pjDest) {
    qWarning() << "初始化目标坐标系失败=>" << pj_strerrno(pj_ctx_get_errno(pjCtx));
    pj_free(pjLonlat);
    pj_ctx_free(pjCtx);
    return PointXY{};
  }

  double x{point.x() * DEG_TO_RAD}, y{point.y() * DEG_TO_RAD};
  auto err = pj_transform(pjLonlat, pjDest, 1, 1, &x, &y, nullptr);
  if (err) {
    qWarning() << "坐标转换失败=>" << point;
    pj_free(pjDest);
    pj_free(pjLonlat);
    pj_ctx_free(pjCtx);
    return PointXY{};
  }

  pj_free(pjDest);
  pj_free(pjLonlat);
  pj_ctx_free(pjCtx);

  return PointXY{x, y};
}

PointXY
CRS::inverse(const PointXY &point) const {
  auto pjCtx = pj_ctx_alloc();
  auto pjLonlat = pj_init_plus_ctx(pjCtx, "+proj=longlat +datum=WGS84 +no_defs");
  if (!pjLonlat) {
    qWarning() << "初始化wgs84坐标系失败=>" << pj_strerrno(pj_ctx_get_errno(pjCtx));
    pj_ctx_free(pjCtx);
    return PointXY{};
  }
  auto pjDest = pj_init_plus_ctx(pjCtx, proj4Def().toStdString().c_str());
  if (!pjDest) {
    qWarning() << "初始化目标坐标系失败=>" << pj_strerrno(pj_ctx_get_errno(pjCtx));
    pj_free(pjLonlat);
    pj_ctx_free(pjCtx);
    return PointXY{};
  }

  double x{point.x()}, y{point.y()};
  auto err = pj_transform(pjDest, pjLonlat, 1, 1, &x, &y, nullptr);
  if (err) {
    qWarning() << "坐标转换失败=>" << point;
    pj_free(pjDest);
    pj_free(pjLonlat);
    pj_ctx_free(pjCtx);
    return PointXY{};
  }

  pj_free(pjDest);
  pj_free(pjLonlat);
  pj_ctx_free(pjCtx);

  return PointXY{x * RAD_TO_DEG, y * RAD_TO_DEG};
}

}