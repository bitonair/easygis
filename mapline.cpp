#include "mapline.h"
#include "maplayer.h"
#include <QPainter>
#include <QDebug>
void EasyGIS::Mapline::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->setPen(this->pen());
    painter->setBrush(this->brush());
    double r=mLayer->resolution();
    for(int i=0;i<(mkt_points.size()-1);i++){
        painter->drawLine(mkt_points[i]/r,mkt_points[i+1]/r);
    }
}

QRectF EasyGIS::Mapline::boundingRect() const
{
    if(this->mkt_points.size()==0){
        return QRectF();
    }
    double r=mLayer->resolution();
    QRectF rect= this->mkt_points.boundingRect();
    return QRectF(rect.x()/r,rect.y()/r,rect.width()/r,rect.height()/r);
}

void EasyGIS::Mapline::append(QPointF mkt_point)
{
    this->mkt_points.append(mkt_point);
}

QPointF &EasyGIS::Mapline::lastMktpoint()
{
    return this->mkt_points.last();
}
