#include <QDebug>
#include <QApplication>
#include <QVector>

#include <provider/tmsprovider.h>
#include <gui/mainwindow.h>

using namespace EasyGIS;

int
main(int argc, char **argv) {
  qRegisterMetaType<TileInfo>("TileInfo");
  qRegisterMetaType<QVector<int>>("QVector<int>");
  QApplication app{argc, argv};

  MainWindow window{};
  window.show();

  return QApplication::exec();
}