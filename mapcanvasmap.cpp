#include <QtCore/QDebug>
#include <QtGui/QPainter>
#include <QtWidgets/QGraphicsSceneMouseEvent>

#include <mapcanvasmap.h>
#include <mapcanvas.h>
#include <maplayer.h>
#include <QDateTime>
namespace EasyGIS {

void
MapCanvasMap::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {
    if (!mLayer->provider().hasContent()) {
    return;
  }
  painter->drawImage(mViewExtent.topLeft(), mLayer->provider().preparedImage());
}

QRectF
MapCanvasMap::boundingRect() const {
  auto width = mViewExtent.size().width();
  auto height = mViewExtent.size().height();
  return mViewExtent + QMarginsF(1024, 1024, 1024, 1024);
}

void MapCanvasMap::setViewExtent(const QRectF &rect) {
  //qDebug() << "更新图层显示对象边界=>" << rect;
  if (rect != mViewExtent) {
    prepareGeometryChange();
    mViewExtent = rect;
  }
}

}

