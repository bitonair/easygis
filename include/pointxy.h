#pragma once

#include <QtCore/QtGlobal>
#include <QtCore/QPoint>
#include <QtCore/QPointF>
#include <QtCore/QDebug>

namespace EasyGIS {
class PointXY {
 public:
  PointXY() = default;
  PointXY(double x, double y) : mX(x), mY(y) {}
  PointXY(const PointXY &other) = default;
  PointXY(PointXY &&other) = default;
  explicit PointXY(const QPointF &other) : mX(other.x()), mY(other.y()) {}
  explicit PointXY(const QPoint &other) : mX(double(other.x())), mY(double(other.y())) {}
  ~PointXY() = default;

 public:
  /**
   * 设置x坐标
   * @param x x轴坐标值
   */
  void setX(double const x) { mX = x; }

  /**
   * 设置y坐标
   * @param y y轴坐标值
   */
  void
  setY(double const y) { mY = y; }

  /**
   * 设置x和y坐标
   * @param x x轴坐标值
   * @param y y轴坐标值
   */
  void set(double const x, double const y) {
    mX = x;
    mY = y;
  }

  /**
   * 获取点的x轴坐标值
   * @return x轴坐标值
   */
  double x() const { return mX; }

  /**
   * 获取点的y轴坐标值
   * @return y轴坐标值
   */
  double y() const { return mY; }

  /**
   * (mX, mY)距离(x,y)的距离
   * @param x x轴坐标
   * @param y y轴坐标
   * @return 两点间距离
   */
  double distance(double x, double y) const;

  /**
   * (mX, mY)距离点p的距离
   * @param p 点坐标
   * @return 两点间距离
   */
  double distance(const PointXY &p) const { return distance(p.mX, p.mY); }

  /**
   * “缩放”点，x轴和y轴乘以scalar
   * @param scalar “缩放”因子
   */
  void multiply(double scalar) {
    mX *= scalar;
    mY *= scalar;
  }

  /**
   * 输出为WKT格式字符串
   * @return WKT数据
   */
  QString toWKT() const;

 public:
  bool operator==(const PointXY &other);
  bool operator!=(const PointXY &other) { return !(*this == other); }
  PointXY &operator=(const PointXY &other);
  PointXY &operator=(const QPointF &other);
  PointXY &operator=(const QPoint &other);

 private:
  double mX;
  double mY;
};

}

QDebug operator<<(QDebug debug, const EasyGIS::PointXY &point);
