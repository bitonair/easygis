#pragma once

#include <QtCore/QRectF>
#include <QtCore/QString>

#include <crs.h>
#include <pointxy.h>

namespace EasyGIS
{
  /**
   * Web莫卡托投影，一般用于web地图投影坐标系。
   * google地图，openstreetmap等，将原点移至了左上角。
   */
  class WebMercator : public CRS
  {
  public:
    /**
     * 返回该坐标系在proj4中的转换公式
     * @return 转换公式的字符串
     */
    QString proj4Cvt() const override;

    /**
     * 返回该坐标系的wkt定义
     * @return 坐标系的wkt定义字符串
     */
    QString wktDef() const override;

    /**
     * 返回该坐标系的proj4定义
     * @return proj4的定义字符串
     */
    QString proj4Def() const override;

    /**
     * 返回该坐标系的整个投影大小
     * @return 投影的整个大小
     */
    QRectF extent() const override;

    /**
     * 将wgs84的经纬度坐标装换为该坐标系坐标
     * @param point 点坐标
     * @return wgs84坐标
     */
    PointXY forward(const PointXY& point) const override;

    /**
     * 将该坐标系坐标转换为wgs84的经纬度坐标
     * @param point 点坐标
     * @return wgs84坐标
     */
    PointXY inverse(const PointXY& point) const override;
  };
}
