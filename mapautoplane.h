#ifndef MAPAUTOPLANE_H
#define MAPAUTOPLANE_H

#include <QGraphicsPixmapItem>
#include <QPointF>
namespace  EasyGIS{
class MapLayer;
class MapAutoplane : public QGraphicsPixmapItem
{
public:
    explicit MapAutoplane(const MapLayer *layer,QPointF pos,QGraphicsItem *parent = nullptr);
    ~MapAutoplane() override = default;
     void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
    QRectF boundingRect() const override;
     void append(QPointF mkt_point);
    void updatePos();
private:
    const MapLayer *mLayer;
    QPointF current_mktpoint;

};
}

#endif // MAPAUTOPLANE_H
