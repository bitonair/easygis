#pragma once

#include <QtGui/QMouseEvent>

namespace EasyGIS {
class MapCanvas;
/**
 * 地图的处理工具
 */
class MapTool {
 public:
  MapTool(MapCanvas *mapCanvas) : mMapCanvas(mapCanvas) {}
  virtual ~MapTool();

 public:
  /**
   * 具体的工具处理程序
   * @param event 鼠标事件
   */
  virtual void execute(QMouseEvent *event) = 0;

  /**
   * 提示将使用工具，此方法中可以为工具的环境做一些准备
   */
  virtual void setup() = 0;

  /**
   * 当工具不使用时，将相关环境还原至原先状态
   */
  virtual void deSetup() = 0;

  /**
   * 获取工具的名称
   * @return 工具名称
   */
  virtual QString id() = 0;

 protected:
  MapCanvas *mMapCanvas;
};

}
