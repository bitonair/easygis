#pragma once

#include <QtCore/QtGlobal>
#include <QtCore/QDebug>
#include <QtCore/QTime>

namespace EasyGIS {
/**
 * 浮点数判断的精度值
 */
const double DoublePrecision{0.00000001};

/**
 * 判断的d1和d2是否符合精度要求
 * @param d1 浮点数
 * @param d2 浮点数
 * @param precision 精度要求
 * @return 小于进度要求返回true，否则返回false
 */
inline bool
isDoubleNearby(double d1, double d2, double precision = DoublePrecision) {
  return qAbs(d1 - d2) < precision;
}

/**
 * 获取min和max间的随机整数
 * @param min 最小值
 * @param max 最大值
 * @return 随机的整数
 */
inline int
randomInt(int min, int max) {
  if (max <= min) {
    qDebug() << "随机数的最大值大于最小值";
    return -1;
  }

  qsrand(static_cast<uint>(QTime{0, 0}.msecsTo(QTime::currentTime())));

  return min + qrand() % (max - min);
}

/**
 * 快速计算2的阶乘
 * @param n 阶数
 * @return 返回结果
 */
inline int
power2(int n) {
  return 1u << static_cast<unsigned>(n);
}

}