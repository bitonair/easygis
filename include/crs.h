#pragma once

#include <QRectF>
#include <QString>

#include <pointxy.h>

namespace EasyGIS
{
  class CRS
  {
  public:
    CRS() = default;
    virtual ~CRS() = default;

  public:
    /**
     * 将wgs84的经纬度坐标装换为该坐标系坐标
     * @param point 点坐标
     * @return wgs84坐标
     */
    virtual PointXY forward(const PointXY& point) const;

    /**
     * 将该坐标系坐标转换为wgs84的经纬度坐标
     * @param point 点坐标
     * @return wgs84坐标
     */
    virtual PointXY inverse(const PointXY& point) const;

    /**
     * 返回该坐标系在proj4中的转换公式
     * @return 转换公式的字符串
     */
    virtual QString proj4Cvt() const = 0;

    /**
     * 返回该坐标系的wkt定义
     * @return 坐标系的wkt定义字符串
     */
    virtual QString wktDef() const = 0;

    /**
     * 返回该坐标系的proj4定义
     * @return proj4的定义字符串
     */
    virtual QString proj4Def() const = 0;

    /**
     * 返回该坐标系的整个投影大小
     * @return 投影的整个大小
     */
    virtual QRectF extent() const = 0;
  };
}
