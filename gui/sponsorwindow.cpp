#include <QtGui/QPixmap>
#include <QtWidgets/QMessageBox>
#include <QtCore/QDebug>

#include <gui/sponsorwindow.h>

SponsorWindow::SponsorWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SponsorWindow) {
  ui->setupUi(this);
  UiSetup();
  Initialize();
}

SponsorWindow::~SponsorWindow() {
  delete ui;
}

/**
 * @brief 窗口相关属性设置
 */
void SponsorWindow::UiSetup() {
  setFixedSize(size());
  setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
  ui->payLbl->setScaledContents(true);
}

/**
 * @brief 窗口初始化
 */
void SponsorWindow::Initialize() {
  connect(ui->aliPayBtn, &QPushButton::clicked, this, &SponsorWindow::AliPayClickedHandle);
  connect(ui->wxPayBtn, &QPushButton::clicked, this, &SponsorWindow::WxPayClickedHandle);

  ui->wxPayBtn->click();
}

/**
 * @brief 支付宝支付
 */
void SponsorWindow::AliPayClickedHandle() {
  QPixmap img{};
  if (!img.load(":/alipay")) {
    QMessageBox::critical(this, "错误", "支付码载入错误！");
    deleteLater();
    return;
  }
  img.scaled(ui->payLbl->size(), Qt::KeepAspectRatio);
  ui->payLbl->setPixmap(img);
}

/**
 * @brief 微信支付
 */
void SponsorWindow::WxPayClickedHandle() {
  QPixmap img{};
  if (!img.load(":/wxpay")) {
    QMessageBox::critical(this, "错误", "支付码载入错误！");
    deleteLater();
    return;
  }
  img.scaled(ui->payLbl->size(), Qt::KeepAspectRatio);
  ui->payLbl->setPixmap(img);
}

